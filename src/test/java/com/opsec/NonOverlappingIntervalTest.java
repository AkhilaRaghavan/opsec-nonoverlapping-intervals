package com.opsec;

import com.opsec.model.Interval;
import com.opsec.util.NonOverlappingInterval;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class NonOverlappingIntervalTest {

    @Test
    public void testGetNonOverlappingIntervals() {
        NonOverlappingInterval nonOverlappingInterval = new NonOverlappingInterval();
        nonOverlappingInterval.add(Interval.of(2, 4))
                .add(Interval.of(5, 8))
                .add(Interval.of(9, 16))
                .add(Interval.of(6, 17));
        List<Interval>  nonOverlappingIntervalIntervals = nonOverlappingInterval.getNonOverlappingIntervals();
        assertEquals("[[2,4], [5,17]]", nonOverlappingIntervalIntervals.toString());
    }

    @Test
    public void testGetNonOverlappingIntervals_duplicateIntervals() {
        NonOverlappingInterval nonOverlappingInterval = new NonOverlappingInterval();
        nonOverlappingInterval.add(Interval.of(2, 4))
                .add(Interval.of(18, 22))
                .add(Interval.of(5, 8))
                .add(Interval.of(9, 16));
        List<Interval>  nonOverlappingIntervalIntervals = nonOverlappingInterval.getNonOverlappingIntervals();
        assertEquals("[[2,4], [5,8], [9,16], [18,22]]", nonOverlappingIntervalIntervals.toString());

        //Updating the interval list, duplicates are merged
        nonOverlappingInterval.add(Interval.of(66, 80))
                .add(Interval.of(16, 54))
                .add(Interval.of(5, 8))
                .add(Interval.of(9, 16));
        nonOverlappingIntervalIntervals = nonOverlappingInterval.getNonOverlappingIntervals();
        assertEquals("[[2,4], [5,8], [9,54], [66,80]]", nonOverlappingIntervalIntervals.toString());
    }

    @Test
    public void testGetNonOverlappingIntervals_testStartIsGreaterThanEnd() {
        NonOverlappingInterval nonOverlappingInterval = new NonOverlappingInterval();
        nonOverlappingInterval.add(Interval.of(2, 4));
        Assertions.assertThrows(IllegalArgumentException.class, () -> nonOverlappingInterval.add(Interval.of(6, 4)));
    }

    @Test
    public void testGetNonOverlappingIntervals_EndisLess() {
        NonOverlappingInterval nonOverlappingInterval = new NonOverlappingInterval();
        Assertions.assertThrows(IllegalArgumentException.class, () -> nonOverlappingInterval.add(Interval.of(2, Integer.MIN_VALUE)));
    }

    @Test
    public void testGetNonOverlappingIntervals_EndisIntMax() {
        NonOverlappingInterval nonOverlappingInterval = new NonOverlappingInterval();
        Assertions.assertThrows(IllegalArgumentException.class, () -> nonOverlappingInterval.add(Interval.of(2, Integer.MAX_VALUE)));
    }

    @Test
    public void testGetNonOverlappingIntervals_StartisIntMax() {
        NonOverlappingInterval nonOverlappingInterval = new NonOverlappingInterval();
        Assertions.assertThrows(IllegalArgumentException.class, () -> nonOverlappingInterval.add(Interval.of(Integer.MIN_VALUE, Integer.MAX_VALUE)));
    }

    @Test
    public void testGetNonOverlappingIntervals_EndIsEqualToStart() {
        NonOverlappingInterval nonOverlappingInterval = new NonOverlappingInterval();
        Assertions.assertThrows(IllegalArgumentException.class, () -> nonOverlappingInterval.add(Interval.of(2, 2)));
    }

    @Test
    public void testGetNonOverlappingIntervals_negativeNumbers() {
        NonOverlappingInterval nonOverlappingInterval = new NonOverlappingInterval();
        Assertions.assertThrows(IllegalArgumentException.class, () -> nonOverlappingInterval.add(Interval.of(-2, 50)));
    }
}
