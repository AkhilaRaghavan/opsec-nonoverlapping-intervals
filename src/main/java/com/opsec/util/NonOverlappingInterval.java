package com.opsec.util;

import com.opsec.model.Interval;

import java.util.*;

public class NonOverlappingInterval implements IntervalMapper {

    private final List<Interval> intervals = new ArrayList<>();

    @Override
    public NonOverlappingInterval add(Interval interval) {
        intervals.add(interval);
        return this;
    }

    /**
     * The method sorts the intervals and then merges
     * the overlapping between the different intervals. The output is
     * a list of sorted non-overlapping intervals
     * @return Returns the non-overlapping intervals
     */
    @Override
    public List<Interval> getNonOverlappingIntervals() {
        Stack<Interval> stack = new Stack<>();
        intervals.stream()
                .sorted(Comparator.comparing(Interval::getStart))
                .forEach(current -> {
                    if (stack.isEmpty()) {
                        stack.push(current);
                    }
                    Interval topInterval = stack.peek();
                    if (current.getStart() <= topInterval.getEnd()) {
                        stack.pop();
                        stack.push(Interval.of(topInterval.getStart(),
                                Math.max(current.getEnd(), topInterval.getEnd())));
                    } else {
                        stack.push(current);
                    }
                });
         return new ArrayList<>(stack);
    }
}
