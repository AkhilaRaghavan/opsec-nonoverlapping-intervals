package com.opsec.util;

import com.opsec.model.Interval;

import java.util.List;

public interface IntervalMapper {

    /**
     * Adds an interval to the interval mapper
     * @param interval Interval with start and end
     */
    NonOverlappingInterval add(Interval interval);

    /**
     * @return Returns a sorted list of non-overlapping intervals
     */
    List<Interval> getNonOverlappingIntervals();
}
