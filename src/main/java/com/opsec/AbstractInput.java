package com.opsec;

import com.opsec.model.Interval;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public abstract class AbstractInput {

    public static List<Interval> readInput(Scanner scanner) {

        List<Interval> intervals = new ArrayList<>();
        System.out.println("Enter number of intervals");
        int intervalCount = scanner.nextInt();
        scanner.nextLine();
        for (int i =0; i < intervalCount; i++) {
            String line = scanner.nextLine();
            String[] split = line.split(",");
            if (split.length != 2) {
                System.out.println("Expected interval format \"start,end\" Example : 2,5");
                System.exit(-1);
            }
            try {
                Interval interval = Interval.of(Integer.parseInt(split[0]), Integer.parseInt(split[1]));
                intervals.add(interval);
            } catch (NumberFormatException e) {
                System.out.println("Expected interval to be of int type : " + e.getMessage());
                System.exit(-1);
            } catch (IllegalArgumentException e) {
                System.out.println(e.getMessage());
                System.exit(-1);
            }
        }
        return intervals;
    }
}
