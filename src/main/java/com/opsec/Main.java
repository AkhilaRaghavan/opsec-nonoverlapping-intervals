package com.opsec;

import com.opsec.model.Interval;
import com.opsec.util.IntervalMapper;
import com.opsec.util.NonOverlappingInterval;

import java.util.List;
import java.util.Scanner;

public class Main extends AbstractInput {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        List<Interval> intervals = readInput(scanner);

        IntervalMapper intervalMapper = new NonOverlappingInterval();
        intervals.forEach(intervalMapper::add);
        System.out.println(intervalMapper.getNonOverlappingIntervals());
    }
}
