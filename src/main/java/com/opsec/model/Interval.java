package com.opsec.model;

public class Interval {

    private final int start;
    private final int end;

    private Interval(int start, int end) {
        validate(start, end);
        this.start = start;
        this.end = end;

    }

    private void validate(int start, int end) {
        if (start < 0 || end < 0) {
            throw new IllegalArgumentException("Negative numbers not allowed!");
        }
        if (start >=  Integer.MAX_VALUE || end >= Integer.MAX_VALUE) {
            throw new IllegalArgumentException("Start/End cannot be greater or equal to " + Integer.MAX_VALUE);
        }
        if (start >= end) {
            throw new IllegalArgumentException("Start is equal to end");
        }
    }

    public static Interval of(int start, int end) {
        return new Interval(start, end);
    }

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    @Override
    public String toString() {
        return "[" + start + "," + end + "]";
    }
}
