
## Non-Overlapping intervals

### Problem Description:

A positive match is when intervals/segments of video downloaded from internet 
matches the reference video. If the intervals are overlapping, this may
lead to incorrect calculation of the match percentage. The task is to merge 
the overlapping segments and return a final set of non-overlapping intervals/segments

### Problem statement:

Given an array of intervals (start, end), Merge overlapping intervals and
return the sorted list of non-overlapping intervals (sorted by start interval).

For example:  
Input : [[2,4],[5,8],[9,16],[6,17]]  
Output : [[2,4], [5,17]]  

0<= start < INT_MAX  
0<= end < INT_MAX  

### How to Run

Run the [Main application](src/main/java/com/opsec/Main.java) and specify the input as described
in the image.  

1. Specify the number of intervals  
2. Specify the interval with start and end delimited by comma. For example : 2,5  

![img.png](img.png)

### Space and TimeComplexity

The above code has a time complexity of O(nlogn),
as the list of intervals are sorted, the actual merging takes linear time.
Space complexity is O(n) , with the usage of stack.

### Extend the code.

The candidate can be asked to extend the code by introducing duration
or length of video. 
The end interval cannot exceed the video duration.

example1:  
Input : [[2,4],[5,8],[9,16],[6,17]]  
Duration of video : 15  
Output : [[2,4], [5,15]] 

example2:  
Input : [[2,4],[5,8],[9,16],[3,17],[6,19]]  
Duration of video : 20  
Output : [[2,4], [5,19]]

0<= start < INT_MAX  
0<= end < INT_MAX  
